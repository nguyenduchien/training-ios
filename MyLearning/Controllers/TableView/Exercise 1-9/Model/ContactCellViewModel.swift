//
//  ContactCellViewModel.swift
//  MyLearning
//
//  Created by Hien Nguyen on 4/20/18.
//  Copyright © 2018 Hien Nguyen D. All rights reserved.
//

import Foundation

final class ContactCellViewModel {
//    var avatarImageView = ""
//    var subname = ""
    var username = ""
    var index = ""
    
    init(username: String, index: String) {
//      var avatarImageView = ""
//      self.subname = subname
        self.username = username
        self.index = index
    }
}
