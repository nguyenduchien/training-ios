//
//  ContactDetailViewModel.swift
//  MyLearning
//
//  Created by Hien Nguyen on 4/20/18.
//  Copyright © 2018 Hien Nguyen D. All rights reserved.
//

import Foundation

final class ContactDetailViewModel {
    var username = ""
    
    init(username: String) {
        self.username = username
    }
}
