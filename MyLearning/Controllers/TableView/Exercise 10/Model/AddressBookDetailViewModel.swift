//
//  AddressBookDetailViewModel.swift
//  MyLearning
//
//  Created by Hien Nguyen on 4/23/18.
//  Copyright © 2018 Hien Nguyen D. All rights reserved.
//

import Foundation
final class AddressBookDetailViewModel {
    
    var username = ""
    
    init(username: String) {
        self.username = username
    }
}
