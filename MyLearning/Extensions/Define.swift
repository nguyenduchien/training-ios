//
//  Define.swift
//  MyLearning
//
//  Created by Hien Nguyen on 5/14/18.
//  Copyright © 2018 Hien Nguyen D. All rights reserved.
//

import Foundation
struct Define {
    struct Title {
        static let home = "Home"
        static let favorite = "Favorite"
        static let map = "Map"
        static let history = "History"
        static let profile = "Profile"
    }
}
